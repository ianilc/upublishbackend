const controller = require('../controllers/test.token')
const jwtAuth = require('../middleware/authJwt')
const canAccess = require('../security/CanAccess')
const signInHelper = require('../helpers/signin.helper')
module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
    app.post("/test", jwtAuth.verifyToken,signInHelper.AuthenticateUserFromToken,controller.test);
};