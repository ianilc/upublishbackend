const express = require('express')
const router = express.Router()

const coverTemplate = require('../controllers/coverTemplates')

router.post('/cover/templates', coverTemplate.fetchAllTemplates)

module.exports = router