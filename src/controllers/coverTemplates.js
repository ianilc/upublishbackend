const coverTemplate = require('../models/coverTemplate')
const ApiResponse = require('../models/ApiResponse')

exports.fetchAllTemplates = async (req, res, next) => {
    const Response = new ApiResponse()
    Response.setTag('/cover/templates')
    Response.setInput(req.body)
    if(req.body.type == undefined || req.body.type == ''){
        Response.setStatus(2)
        Response.setDescription('Could not find type')
        Response.setResult(null)
        return res.send(Response)
    }
    try{
        const templates = await coverTemplate.find({
            status: 1,
            type: req.body.type
        }).limit(10)
        //Sorting?
        Response.setStatus(1)
        Response.setDescription('Fetched templates')
        Response.setResult({ templates })
        return res.send(Response)
    }
    catch(err){
        Response.setStatus(0)
        Response.setDescription(err.message)
        Response.setResult(null)
        return res.send(Response)
    }
}