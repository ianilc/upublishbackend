const signUp = require("../signup/signup");
const ApiResponse = require('../models/ApiResponse');

exports.socialLogin = (req,res) => {
    signUp.querySignUp(req.body).then(function(value) {
        res.send(new ApiResponse(1,'',value,'','socialLogin'))
      }).catch(function(value){
        res.send(new ApiResponse(0,value,{},'','socialLogin'))
      })  
}