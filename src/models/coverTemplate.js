const mongoose = require('mongoose')

const template = mongoose.Schema({
    type: {
        type: String, //Can be FRONT or BACK
        required: true
    },
    canvasData: {
        type: {
            texts: [{
                text: { type: String },
                x: { type: Number },
                y: { type: Number },
                fontSize: { type: Number },
                fontFamily: { type: String },
                width: { type: Number },
                padding: { type: Number },
                fontStyle: { type: String },
                align: { type: String },
                stroke: { type: String },
                fill: { type: String },
                strokeWidth: { type: Number },
                lineJoin: { type: String },
                scaleX: { type: Number },
                scaleY: { type: Number },
                draggable: { type: Boolean },
            }],
            images: [{
                x: { type: Number },
                y: { type: Number },
                isBackground: { type: Boolean },
                height: { type: Number },
                width: { type: Number },
                imageURL: { type: String }
            }]
        },
        default: { texts: [], images: [] }
    },
    thumbnail: {
        type: String,
        required: true
    },
    createdOn: {
        type: Date,
        required: true
    },
    createdBy: {
        type: mongoose.Types.ObjectId,
        ref: 'user',
        required: false
    },
    lastModifiedOn: {
        type: Date,
        required: true
    },
    lastModifiedBy: {
        type: mongoose.Types.ObjectId,
        ref: 'user',
        required: false
    },
    status: {
        type: Number,
        required: true,
        default: true
    }
})

module.exports = mongoose.model('coverTemplate', template, 'cover_templates')