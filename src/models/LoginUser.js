module.exports = (sequelize , Sequelize) => {
const LoginUser = sequelize.define('user', {
    UserId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    UserName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    UserContactNo: {
        type: Sequelize.BIGINT,
        allowNull: false
    },
    UserCity: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserState: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserCountry: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserAddress: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserEmail: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserPassword: {
        type: Sequelize.STRING,
        allowNull: false
    },
    LastLoggedInDate: {
        type: Sequelize.DATE,
        allowNull: true
    },
    Status: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    DeviceId: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserToken: {
        type: Sequelize.STRING,
        allowNull: true
    },
    UserType: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    Department: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    OTP: {
        type: Sequelize.STRING,
        allowNull: true
    },
    OtpCreatedDate: {
        type: Sequelize.DATE,
        allowNull: true
    },
    Message: {
        type: Sequelize.STRING,
        allowNull: true
    },
    AssignIndustrySegment: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    LastModifiedBy: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    LastModifiedOn: {
        type: Sequelize.DATE,
        allowNull: false
    },
    CreatedBy: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    CreatedOn: {
        type: Sequelize.DATE,
        allowNull: false
    },
    ADUserId: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Mobile_OTP: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Mobile_OTP_ExpireOn: {
        type: Sequelize.DATE,
        allowNull: true
    },
    Email_OTP: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Email_OTP_ExpireOn: {
        type: Sequelize.DATE,
        allowNull: true
    },
    MobileverificationDate: {
        type: Sequelize.DATE,
        allowNull: true
    },
    EmailverificationDate: {
        type: Sequelize.DATE,
        allowNull: true
    },
    LastSiteId: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    LastOrganizationId: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    LastRoleId:{
        type: Sequelize.INTEGER,
        allowNull: true
    },
    LastLanguageCode: {
        type: Sequelize.STRING,
        allowNull: true,
        default: 'en'
    }
}, {
    freezeTableName: true,
    tableName: 'user',
    timestamps: false
})
return LoginUser
};
//module.exports = LoginUser;