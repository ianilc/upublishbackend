module.exports = class ApiResponse{
    constructor (status, input, result, token, tag, description){
        this.status= status;
        this.token = token;
        this.result = result;
        this.tag = tag;
        this.input = input;
        this.description = description;
    }

    //Status 0 is for failure in API
    //Status 1 for success in API
    //Status 2 for Access Denied
    setStatus(StatusCode){
        this.status = StatusCode;
    }

    setToken(Token){
        this.token = Token;
    }

    setResult(Result){
        this.result = Result;
    }

    setTag(Tag){
        this.tag = Tag;
    }

    setInput(Input){
        this.input = Input;
    }

    setDescription(Description){
        this.description = Description;
    }

    getApiResponse(){
        return this;
    }
}