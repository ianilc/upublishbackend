/* eslint-disable no-redeclare */
exports.canAccess = (UserInfo, ModuleCode, FunctionCode) => {
	var OrgID = UserInfo.CurrentOrganizationID
	if(UserInfo.AssignedOrgs[OrgID]){
		//Check for site access
		var AccessFound = false
		var AllSites = UserInfo.AssignedOrgs[OrgID].Sites
		for(var i = 0; i < AllSites.length; i++){
			if(AllSites[i].SiteID == UserInfo.CurrentSiteID && AllSites[i].RoleID == UserInfo.CurrentRoleID){
				if(AllSites[i].Access[ModuleCode]){
					if(AllSites[i].Access[ModuleCode].includes(FunctionCode)){
						AccessFound = true
						return true
					}
				}
			}
		}
		if(!AccessFound){
			return false
		}
	}
	else{
		return false
	}
}

exports.GetDataSecurity = (UserInfo, Type, AliasArray) => {
	/*
    @params
    UserInfo - req.UserInfo object

    Type - 1 MySQL
    Type - 2 MongoDB 
	Type - 3 Elastic search
	
	
    AliasArray - Required if MySQL query - Array [Organization_Alias, Site_ALias]

    @Logic
    If CurrentSiteID == 0,
        Search across all sites
    Else 
        Search in specific organization and Site
    */
   
	switch(Type) {
	case 1:
		var query = []
		query.push(AliasArray[0] + '.OrganizationID = ' + UserInfo.CurrentOrganizationID)
		if(UserInfo.CurrentSiteID != 0){
			query.push(AliasArray[1] + '.SiteID = ' + UserInfo.CurrentSiteID)
		}
		return (query.join(' and '))
	case 2:
		var query = {}
		query.OrganizationID = UserInfo.CurrentOrganizationID
		if(UserInfo.CurrentSiteID != 0){
			query.SiteID = UserInfo.CurrentSiteID
		}
		return query
	case 3:
		var query = []
		query.push({ 'organizationid': UserInfo.CurrentOrganizationID })
		if(UserInfo.CurrentSiteID != 0){
			query.push({ 'siteid': UserInfo.CurrentSiteID })
		}
		return query
	default:
		return ''
	}
}