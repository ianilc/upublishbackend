const mysql             = require("../util/mysql");
const verifysocialtoken = require("./verifysocialtoken.js");
const jwt               = require("jsonwebtoken");
const config            = require("../config/config");
const signInHelper      = require("../helpers/signin.helper")

async function querySignUp (data) {
 let isVerified = true
 try {
   if(data.provider==='GOOGLE')
  await verifysocialtoken.verifyGoogle(data.idToken);
   else if(data.provider==='FACEBOOK')
   await verifysocialtoken.getFacebookUserData(data.authToken);
   else
   throw Error('not a valid platform')
 } catch (error) {
    console.log(error)
    isVerified = false
 }
 if(!isVerified){
   throw Error('not a valid user')
 }
const connection = await mysql.connection();
  try {
    console.log("at querySignUp...");
    await connection.query("START TRANSACTION");
    let usernameNo = await connection.query("SELECT COUNT (*) FROM user WHERE (GoogleId = ? || FacebookId = ?) AND UserEmail = ? ", [data.id,data.id,data.email]);
    let userInfo = {
        UserName: data.name,
        UserEmail: data.email,
        Status:1
      };
    data.provider==='GOOGLE' ? ((userInfo.GoogleId = data.id)&&(userInfo.GooglePhotoURL = data.photoUrl)) : (userInfo.FacebookId = data.id)&&(userInfo.FacebookPhotoURL = data.photoUrl)
    if (usernameNo[0]["COUNT (*)"] == 0) {
        await connection.query("INSERT INTO user SET ?", userInfo);
        let getUserInfo = await getUser(data,connection);
        await addTokenToRes(getUserInfo[0])
        await connection.query("COMMIT");
        return getUserInfo[0] 
    }else{
        let getUserInfo = await getUser(data,connection);
        await addTokenToRes(getUserInfo[0])
        return getUserInfo[0]
    }
    
  } catch (err) {
    await connection.query("ROLLBACK");
    console.log('ROLLBACK at querySignUp', err);
    throw err;
  } finally {
    await connection.release();
  }
}

async function getUser(data,connection){
  return await connection.query("SELECT UserEmail as userEmail,UserName as userName,UserId as userId FROM user WHERE (GoogleId = ? || FacebookId = ?) AND UserEmail = ? ", [data.id,data.id,data.email]);
}

async function addTokenToRes(getUserInfo){
  var getRole = await signInHelper.getUserSiteAccess(getUserInfo.userId)
  getRole = signInHelper.AddAllSitesInOrgs(getRole)
  let token = jwt.sign({ id: getUserInfo.userId,roles:getRole }, config.secret, {
    expiresIn: 60 // 1 minute
  });
  getUserInfo.token = token
  return getUserInfo
}

exports.getUserSiteAccessFromToken = (UserInfo) => {
	var getsiteId = UserInfo.CurrentSiteID
	var getORgId = UserInfo.CurrentOrganizationID
	var getAllsite = UserInfo.AssignedOrgs[getORgId]
	var orglength = getAllsite.Sites.length
	for (var i = 0; i < orglength; i++) {
		if (getAllsite.Sites[i].SiteID == getsiteId) {
			var getAccess = getAllsite.Sites[i].Access
			return getAccess;
		}
	}
	return false;
}

module.exports = { querySignUp };