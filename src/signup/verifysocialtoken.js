const {OAuth2Client} = require('google-auth-library');
const config = require('../config/config');
const client = new OAuth2Client(config.GOOGLE_CLIENT_ID);
const axios = require('axios');

async function verifyGoogle(token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: config.GOOGLE_CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
}
async function getFacebookUserData(accesstoken) {
    console.log("getFacebookUserData getFacebookUserData getFacebookUserData ",accesstoken)
    const { data } = await axios({
      url: 'https://graph.facebook.com/me',
      method: 'get',
      params: {
        fields: ['id', 'email', 'first_name', 'last_name'].join(','),
        access_token: accesstoken,
      },
    });
    return data;
  };
module.exports = { verifyGoogle, getFacebookUserData };