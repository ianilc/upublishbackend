const mysql = require("mysql");
const Sequelize= require('sequelize')

let {MYSQL_DB,MYSQL_HOST,MYSQL_PASSWORD,MYSQL_USER}  = require("../config/config")

let dbConfig = {
   connectionLimit: 100, // default 10
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DB
};
const pool = mysql.createPool(dbConfig);
const connection = () => {
  return new Promise((resolve, reject) => {
  pool.getConnection((err, connection) => {
    if (err) reject(err);
    console.log("MySQL pool connected: threadId " + connection.threadId);
    const query = (sql, binding) => {
      return new Promise((resolve, reject) => {
         connection.query(sql, binding, (err, result) => {
           if (err) reject(err);
           resolve(result);
           });
         });
       };
       const release = () => {
         return new Promise((resolve, reject) => {
           if (err) reject(err);
           console.log("MySQL pool released: threadId " + connection.threadId);
           resolve(connection.release());
         });
       };
       resolve({ query, release });
     });
   });
 };
const query = (sql, binding) => {
  return new Promise((resolve, reject) => {
    pool.query(sql, binding, (err, result, fields) => {
      if (err) reject(err);
      resolve(result);
    });
  });
};

const sequelize=new Sequelize(MYSQL_DB, MYSQL_USER, MYSQL_PASSWORD, {
	dialect:'mysql',
	host:MYSQL_HOST
})

const LoginUser = require("../models/LoginUser")(sequelize, Sequelize);
module.exports = { pool, connection, query, sequelize,Sequelize,LoginUser };