const jwt = require("jsonwebtoken");
const config = require("../config/config");
const mysql = require("../util/mysql");
const LoginUser = require('../util/mysql').LoginUser;
const ApiResponse = require('../models/ApiResponse')
exports.AuthenticateUserFromToken = async (req, res, next) => {
	try {
        let token = req.headers["x-access-token"];
	    var userInfo = jwt.verify(token, config.secret)
	}
	catch (err) {
		console.log(err)
		const Response = new ApiResponse()
        Response.setStatus(2)
        Response.setMessage('Could not verfiy token')
		Response.setResult(req.body)
		return res.send(Response)
	}
	const FoundUser = await LoginUser.findOne({
		where: {
			UserId: userInfo.id
		},
		attributes: ['UserId', 'UserName', 'UserEmail', 'LastRoleId', 'LastSiteId', 'LastOrganizationId', 'LastLanguageCode', 'Status']
	})
	if (FoundUser == null) {
		const Response = new ApiResponse()
		Response.setStatus(2)
		Response.setMessage('NOT_FOUND')
		Response.setResult(null)
		return res.send(Response)
	}
	if (FoundUser && FoundUser.Status == -1) {
		const Response = new ApiResponse()
		Response.setStatus(1)
		Response.setMessage('INACTIVE')
		Response.setResult(null)
		return res.send(Response)
	}
	try {
		var getRole = await this.getUserSiteAccess(FoundUser.UserId)
		getRole = this.AddAllSitesInOrgs(getRole)
		userInfo.isLoggedIn = true
		userInfo.userId = FoundUser.UserId
		userInfo.UserName = FoundUser.UserName
		userInfo.UserEmail = FoundUser.UserEmail
		userInfo.UserLanguage = FoundUser.LastLanguageCode
		if (FoundUser.LastSiteId >= 0 && FoundUser.LastOrganizationId) {
			//userInfo.CurrentRoleID = FoundUser.LastRoleId;
			userInfo.CurrentSiteID = FoundUser.LastSiteId
			userInfo.CurrentOrganizationID = FoundUser.LastOrganizationId
			var IsFound = false
			for (var i = 0; i < getRole.length; i++) {
				if (getRole[i].SiteID == FoundUser.LastSiteId &&
					getRole[i].OrganizationId == FoundUser.LastOrganizationId) {
					userInfo.CurrentSiteName = getRole[i].siteName
					userInfo.CurrentOrganizationName = getRole[i].Name
					userInfo.CurrentRoleName = getRole[i].RoleName
					userInfo.CurrentRoleID = getRole[i].RoleId
					IsFound = true
					break
				}
			}
			if (!IsFound) {
				for (var i = 0; i < getRole.length; i++) {
					if (getRole[i].IsDefault) {
						userInfo.CurrentRoleID = getRole[i].RoleId
						userInfo.CurrentRoleName = getRole[i].RoleName
						userInfo.CurrentSiteID = getRole[i].SiteID
						userInfo.CurrentSiteName = getRole[i].siteName
						userInfo.CurrentOrganizationID = getRole[i].OrganizationId
						userInfo.CurrentOrganizationName = getRole[i].Name
						LoginUser.findOne({
							where: { UserId: FoundUser.UserId }
						})
							.then(async (user) => {
								await user.update({
									LastSiteId: getRole[i].SiteID,
									LastOrganizationId: getRole[i].OrganizationId
								})
							})
						break
					}
				}
			}
		}
		else {
			//Find the first IsDefault site and assign it to user
			for (var i = 0; i < getRole.length; i++) {
				if (getRole[i].IsDefault) {
					userInfo.CurrentRoleID = getRole[i].RoleId
					userInfo.CurrentRoleName = getRole[i].RoleName
					userInfo.CurrentSiteID = getRole[i].SiteID
					userInfo.CurrentSiteName = getRole[i].siteName
					userInfo.CurrentOrganizationID = getRole[i].OrganizationId
					userInfo.CurrentOrganizationName = getRole[i].Name
					LoginUser.findOne({
						where: { UserId: FoundUser.UserId }
					})
						.then(async (user) => {
							await user.update({
								LastSiteId: getRole[i].SiteID,
								LastOrganizationId: getRole[i].OrganizationId
							})
						})
					break
				}
			}
		}
		//userInfo.LastLogin = CommonController.formatAMPM(new Date())
		userInfo.AssignedOrgs = await OrganizationHeirarichy(getRole)
		const Response = new ApiResponse()
		Response.setStatus(1)
		Response.setMessage('FOUND')
		Response.setResult(FoundUser)
		return res.send(Response)
	}
	catch (err) {
		console.log(err)
		const Response = new ApiResponse()
		Response.setStatus(0)
		Response.setMessage(err.message)
		Response.setResult(null)
		return res.send(Response)
	}
}
exports.getUserSiteAccess = async (userId) => {
    const connection = await mysql.connection();
	return new Promise(async (resolve, reject) => {
		try {
			connection.query('SELECT srl.RoleId, rl.RoleName, si.SiteID, si.siteName, si.IsDefault, si.OrganizationId, org.Name from siterolelink srl'
				+ ' INNER JOIN role rl on rl.RoleId = srl.RoleId'
				+ ' INNER JOIN site si on si.SiteID = srl.AssSiteId'
				+ ' INNER JOIN organization org on si.OrganizationId = org.OrganizationID'
				+ ' where srl.UserId = ' + userId + ' and srl.status = 1;', (err, result, field) => {
				if (err) {
					console.log('User site access errro')
					console.log(err)

					reject(err.errno)
				}
				resolve(result)
			})
			/* select distinct fn.FunctionCode,md.ModuleCode,rl.RoleId,us.UserId,us.SiteId as SiteId, us.OrgnizationId as OrganizationId from user us "
                + " left join userrolelink url on us.UserId= url.UserId left join role rl on url.RoleId= rl.RoleId "
                + " left join siterolelink slnk on rl.RoleId=slnk.RoleId left join site st on slnk.AssSiteId= st.LocationId "
                + " left join rolemodulefuctionlink rlm on rl.RoleId=rlm.RoleId left join module md on rlm.ModuleId= md.ModuleId "
                + " left join function fn on rlm.FunctionId= fn.FunctionId where us.UserId=" + userId + "; */
		}
		catch (err) {
			reject(err)
		}
	})
}
exports.AddAllSitesInOrgs = (AllAccess) => {
	var AllSitesOrgs = []
	var AllAccessCopy = Object.assign([], AllAccess)
	for (var i = 0; i < AllAccess.length; i++) {
		if (AllAccess[i].RoleId == 5 && !AllSitesOrgs.includes(AllAccess[i].OrganizationId)) {
			AllAccessCopy.push({
				RoleId: AllAccess[i].RoleId,
				RoleName: AllAccess[i].RoleName,
				SiteID: 0, //Means all sites
				siteName: 'All sites',
				IsDefault: 0,
				OrganizationId: AllAccess[i].OrganizationId,
				Name: AllAccess[i].Name
			})
			AllSitesOrgs.push(AllAccess[i].OrganizationId)
		}
	}
	return AllAccessCopy
}

OrganizationHeirarichy = (OrgData) => {
	return new Promise(async (resolve, reject) => {
		var result = {}
		for (var i = 0; i < OrgData.length; i++) {
			var OrgID = OrgData[i].OrganizationId
			if (!result[OrgID]) {
				//Push data in this org ID
				result[OrgID] = {}
				result[OrgID].Sites = []
			}
			result[OrgID].OrganizationID = OrgData[i].OrganizationId
			result[OrgID].OrganizationName = OrgData[i].Name
			var SiteData = {}
			SiteData.SiteID = OrgData[i].SiteID
			SiteData.SiteName = OrgData[i].siteName
			SiteData.RoleID = OrgData[i].RoleId
			SiteData.RoleName = OrgData[i].RoleName
			//Get Access for this RoleID
			var RoleAccess = await GetAccessFromRoleID(OrgData[i].RoleId)
			SiteData.Access = {}
			for (var j = 0; j < RoleAccess.length; j++) {
				if (!SiteData.Access[RoleAccess[j].ModuleCode]) {
					SiteData.Access[RoleAccess[j].ModuleCode] = []
				}
				SiteData.Access[RoleAccess[j].ModuleCode].push(RoleAccess[j].FunctionCode)
			}
			result[OrgID].Sites.push(SiteData)
		}
		resolve(result)
	})
}

GetAccessFromRoleID = async (RoleID) => {
	const connection = await mysql.connection();
	return new Promise(async (resolve, reject) => {
		try {
			connection.query('SELECT rmfl.ModuleId, m.ModuleCode, rmfl.FunctionId, f.FunctionCode'
				+ ' FROM deepsense.rolemodulefuctionlink rmfl'
				+ ' INNER JOIN module m on m.ModuleId = rmfl.ModuleId'
				+ ' INNER JOIN deepsense.function f on f.FunctionId = rmfl.FunctionId'
				+ ' where RoleId = ' + RoleID + ' and rmfl.Status = 1;', (err, result, field) => {
				if (err) {
					reject(err.errno)
				}
				resolve(result)
			})
		}
		catch (err) {
			reject(err)
		}
	})
}