require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const path = require('path')
const mongoose = require('mongoose')
const routes = require('./src/routes/routes')
const serveStatic = require('serve-static')
app.use(cors())

app.use(express.static(path.join(__dirname, 'src', 'coverImages')))

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }))
app.use(bodyParser.json({ limit: '50mb' }))
require('./src/routes/auth.routes')(app);
require('./src/routes/user.routes')(app);
app.use(routes)



app.listen(3000,() =>{
    console.log('Server is up and runnig on port 3000.')
    var ConnectionString = 'mongodb://localhost:27017/uPublish?readPreference=primary&ssl=false'
    mongoose.connect(ConnectionString, { useFindAndModify: false, useUnifiedTopology: true, useNewUrlParser: true })
})